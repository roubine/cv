.FORCE:

all: .FORCE
	pdflatex $(NAME).tex

full: .FORCE
	python var.py
	pdflatex $(NAME).tex
	make bib
	pdflatex $(NAME).tex
	pdflatex $(NAME).tex

bib: .FORCE
	# for f in $(wildcard *.bbl) ; do \
	#  	bibtex $${f%.bbl} ; \
	# done
	bibtex $(NAME)

age:
	bash age.bash > age.tex

clean:
	@rm -fv *.blg \
         *.bbl \
         *.mtc0 \
         *.aux* \
         *.out \
         *.toc \
         *.log \
         *.mtc* \
         *.maf*\
         *.pdf \
	 *-blx.bib \
	 *.run.xml
