import requests

# get biblio metrics from eroubin.xyz
response = requests.get("https://eroubin.xyz/biblio")
payload = response.json()

# create graph citations data
with open('var_citations.txt', 'w+') as f:
    for c in payload["citations"]["graph"]:
        f.write(f'{c["year"]} {c["citations"]}\n')


# create graph article data
with open('var_articles.txt', 'w+') as f:
    for y, n in payload["production"]["Articles"].items():
        if y != "total":
            f.write(f'{y} {n}\n')

# define biblio variables
with open('var.tex', 'w+') as f:
    for k in payload["citations"]["table"]:
        if 'citations' in k:
            f.write(f'\\newcommand{{\\totalcitations}}{{{k["citations"]["all"]}}}\n')
        if 'h_index' in k:
            f.write(f'\\newcommand{{\\hindex}}{{{k["h_index"]["all"]}}}\n')
        if 'i10_index' in k:
            f.write(f'\\newcommand{{\\iindex}}{{{k["i10_index"]["all"]}}}\n')

    f.write(f'\\newcommand{{\\totalarticles}}{{{payload["production"]["Articles"]["total"]}}}\n')
    f.write(f'\\newcommand{{\\totalbooks}}{{{payload["production"]["Books"]["total"]}}}\n')
    f.write(f'\\newcommand{{\\totalconf}}{{{payload["production"]["Conferences"]["total"]}}}\n')

