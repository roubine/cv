%-----------------------------------------------------------------------------------------------------------------------------------------------%
%	The MIT License (MIT)
%
%	Copyright (c) 2015 Jan Küster
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%
%
%-----------------------------------------------------------------------------------------------------------------------------------------------%


%============================================================================%
%
%	DOCUMENT DEFINITION
%
%============================================================================%

%we use article class because we want to fully customize the page and dont use a cv template
\documentclass[10pt,A4]{article}


%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

%we use utf8 since we want to build from any machine
\usepackage[utf8]{inputenc}

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xifthen}

%----------------------------------------------------------------------------------------
%	FONT
%----------------------------------------------------------------------------------------

% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
\usepackage[default]{raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
%\usepackage[thin]{roboto}

% set font default
\renewcommand*\familydefault{\sfdefault}
\usepackage[T1]{fontenc}

% more font size definitions
\usepackage{moresize}


%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

%debug page outer frames
%\usepackage{showframe}


%define page styles using geometry
\usepackage[a4paper]{geometry}

% for example, change the margins to 2 inches all round
\geometry{top=1cm, bottom=1cm, left=1cm, right=1cm}

%use customized header
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}

%less space between header and content
\setlength{\headheight}{-5pt}


%customize entries left, center and right
\lhead{}
% \chead{\small{Emmanuel Roubin $\cdot$ \textcolor{acccol}{\textbf{www.eroubin.xyz}}} $\cdot$  p. \thepage/\pageref{LastPage}}
\chead{}
\rhead{}
\rfoot{}
% \cfoot{}
\cfoot{\small{Emmanuel Roubin $\cdot$ \textcolor{acccol}{\href{https://www.eroubin.xyz}{\textbf{www.eroubin.xyz}}}} $\cdot$  p. \thepage/\pageref{LastPage}}
\lfoot{}


%indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%----------------------------------------------------------------------------------------

%for layouting tables
\usepackage{multicol}
\usepackage{multirow}

%extended aligning of tabular cells
\usepackage{array}
\usepackage{tabu}
\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0pt}}p{#1}}%


%----------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%----------------------------------------------------------------------------------------

%for header image
\usepackage{graphicx}

%for floating figures
\usepackage{wrapfig}
\usepackage{float}
%\floatstyle{boxed}
%\restylefloat{figure}

%for drawing graphics
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{shapes, backgrounds,mindmap, trees}


%----------------------------------------------------------------------------------------
%	Color DEFINITIONS
%----------------------------------------------------------------------------------------

\usepackage{color}

%accent color
\definecolor{acccol}{RGB}{255,150,50}

%dark background color
\definecolor{bgcol}{RGB}{110,110,110}

%light background / accent color
\definecolor{softcol}{RGB}{225,225,225}

%close to font color
\definecolor{fontcol}{RGB}{100,100,100}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\usepackage[hidelinks,breaklinks]{hyperref}
\hypersetup{
    pdftitle={Emmanuel Roubin | CV},
    pdfsubject={Emmanuel Roubin | Curriculum Vitae},
    pdfauthor={Emmanuel Roubin},
}

%----------------------------------------------------------------------------------------
%	BIBTEX
%----------------------------------------------------------------------------------------
\usepackage[backend=bibtex,style=numeric,sorting=none]{biblatex}
\bibliography{eroubin.bib}

%============================================================================%
%
%
%	DEFINITIONS
%
%
%============================================================================%

%----------------------------------------------------------------------------------------
% 	HEADER
%----------------------------------------------------------------------------------------

% remove top header line
\renewcommand{\headrulewidth}{0pt}

%remove botttom header line
\renewcommand{\footrulewidth}{0pt}

%remove pagenum
% \renewcommand{\thepage}{}

%remove section num
% \renewcommand{\thesection}{}

%----------------------------------------------------------------------------------------
% 	Latin
%----------------------------------------------------------------------------------------
\def\insitu{\textit{in situ}}

%----------------------------------------------------------------------------------------
% 	Live data
%----------------------------------------------------------------------------------------
\input{var}

%----------------------------------------------------------------------------------------
% 	ARROW GRAPHICS in Tikz
%----------------------------------------------------------------------------------------

% a six pointed arrow
% \newcommand{\tzmybullet}{(0,0) -- (0.2,0) -- (0.3,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.1,0.2) -- cycle;}
\newcommand{\tzmybullet}{(0,0) -- (0.2,0) -- (0.4,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.2,0.2) -- cycle;}

% include the arrow into a tikz picture
% param1: fill color
%
\newcommand{\mybullet}[1]
{\begin{tikzpicture}[scale=0.58]
	 \filldraw[fill=#1!100,draw=#1!100!black]  \tzmybullet
 \end{tikzpicture}
}

% 3 arrows in a row
% param1: fill color
%
\newcommand{\mylargebullet}[1]
{
	\mybullet{#1} \hspace{-8pt} \mybullet{#1} \hspace{-8pt} \mybullet{#1}
}

% strong keyword
\newcommand{\strong}[1]
{\textbf{#1}}

%----------------------------------------------------------------------------------------
%	custom sections
%----------------------------------------------------------------------------------------

% create a coloured box with arrow and title as cv section headline
% param 1: section title
%
\newcommand{\cvsection}[1]
{
\colorbox{bgcol}{\mystrut \makebox[1\linewidth][l]{
\mylargebullet{acccol} \textcolor{white}{\textbf{#1}}\hspace{4pt}
}}\\
}

%create a coloured arrow with title as cv meta section section
% param 1: meta section title
%
\newcommand{\metasection}[2]
{
\begin{tabular*}{1\textwidth}{p{3cm} p{10.4cm}}
	\mybullet{bgcol} \normalsize{\textcolor{acccol}{#1}}&#2\\[12pt]
\end{tabular*}
}

%create a coloured arrow with title as cv meta section section
% param 1: meta section title
%
\newcommand{\singlebullet}[2]
{\centering
  \parbox[c]{0.95\textwidth}{\mybullet{bgcol} \textcolor{acccol}{#1}\hspace{10pt}#2}\\[6pt]
}

%----------------------------------------------------------------------------------------
%	 CV EVENT
%----------------------------------------------------------------------------------------

% creates a stretched box as cv entry headline followed by two paragraphs about
% the work you did
% param 1:	event time i.e. 2014 or 2011-2014 etc.
% param 2:	event name (what did you do?)
% param 3:	institution (where did you work / study)
% param 4:	what was your position
% param 5:	some words about your contributions
%
\newcommand{\cvevent}[5]
{
\vspace{8pt}
	\begin{tabular*}{1\textwidth}{p{2cm}  p{11cm} x{4.5cm}}
 \textcolor{bgcol}{#1}& \textbf{#2} & \vspace{2.5pt}\textcolor{acccol}{#3}

	\end{tabular*}
\vspace{-12pt}
\textcolor{softcol}{\hrule}
\vspace{6pt}
\begin{tabular*}{1\textwidth}{p{0.5cm} p{17cm}}
  &		 \mybullet{bgcol} \textit{#4}\\
  &		 \mybullet{bgcol} \textit{#5}
\end{tabular*}~\\[2pt]
}

\newcommand{\cveventsingle}[4]
{
\vspace{8pt}
	\begin{tabular*}{1\textwidth}{p{2cm} p{9cm} x{6.5cm}} % adds up to 17.5
 \textcolor{bgcol}{#1}& \textbf{#2} & \vspace{2.5pt}\textcolor{fontcol}{#3}

	\end{tabular*}
\vspace{-12pt}
\textcolor{softcol}{\hrule}
\vspace{6pt}

\begin{tabular*}{1\textwidth}{p{0.5cm} p{17cm}}
  &	\small \textit{#4}
\end{tabular*}~\\[2pt]

}

\newcommand{\cveventsimple}[3]
{
  \vspace{8pt}
  \begin{tabular*}{1\textwidth}{p{2cm} p{15.5cm}} % adds up to 17.5
    \textcolor{bgcol}{#1}& \textbf{#2} \hspace{2pt} \textit{#3}
  \end{tabular*}~\\[2pt]
}


% creates a stretched box as
\newcommand{\cveventmeta}[2]
{
	\mbox{\mystrut \hspace{87pt}\textit{#1}}\\
	#2
}

%----------------------------------------------------------------------------------------
% CUSTOM STRUT FOR EMPTY BOXES
%----------------------------------------- -----------------------------------------------
\newcommand{\mystrut}{\rule[-.2\baselineskip]{0pt}{\baselineskip}}

%----------------------------------------------------------------------------------------
% CUSTOM LOREM IPSUM
%----------------------------------------------------------------------------------------
\newcommand{\lorem}
{Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus.}



%============================================================================%
%
%
%
%	DOCUMENT CONTENT
%
%
%
%============================================================================%
\begin{document}


%use our custom fancy header definitions
\pagestyle{fancy}


%---------------------------------------------------------------------------------------
%	TITLE HEADLINE
%----------------------------------------------------------------------------------------
\null
\vspace{-5pt}
\vspace{-18pt}

% use this for multiple words like working titles etc.
\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\hspace{46pt}\Huge{\textcolor{white}{\textsc{Emmanuel Roubin}} } \textcolor{acccol}{\rule[-1mm]{1mm}{0.9cm}} \parbox[b]{5cm}{   \normalsize{ \textcolor{white}{{Maître de conférence}}}\\
\normalsize{ \textcolor{white}{{Habilité à dirigé des recherches}}}}
}}

% use this for single words, e.g. CV or RESUME etc.
% \hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\HUGE{\textcolor{white}{\textsc{Jan Küster}} } \textcolor{acccol}{\rule[-1mm]{1mm}{0.9cm}} \HUGE{\textcolor{white}{\textsc{Resume}} } }}


%---------------------------------------------------------------------------------------
%	SUBTITLE
%----------------------------------------------------------------------------------------
% \begin{center}
% 	coucou
% \end{center}
% \vspace{-20pt}


%----------------------------------------------------------------------------------------
%	HEADER IMAGE
%----------------------------------------------------------------------------------------

\begin{figure}[!h]
	\begin{flushright}
		% \includegraphics[trim=left bottom right top, clip]{file}
		% should be 339px x 349px
		\includegraphics[trim= 0 8 0 5,clip,width=0.2\linewidth]{profile.png}	%trimming relative to image size!
	\end{flushright}
\end{figure}

%---------------------------------------------------------------------------------------
%	META SECTION
%----------------------------------------------------------------------------------------

\vspace{-130pt}

\metasection{Rattachments}{%
	U\textcolor{fontcol}{nivsersité} G\textcolor{fontcol}{renoble} A\textcolor{fontcol}{lpes} (2015)
	\par\textcolor{fontcol}{Laboratoire 3SR}\hspace{0.3cm}%
	IUT1 Génie Civil et Construction Durable%
}
\metasection{Diplômes}{%
	Doctorat (ENS Cachan, 2013)%
	\par \textcolor{fontcol}{Habilité à Diriger des Recherche (UGA, 2022)}%
}
\metasection{Recherche}{%
	Comportement des géomatériaux aux échelles fines\hspace{0.3cm}%
	\textcolor{fontcol}{Dialogue expériences / simulations}\hspace{0.3cm}%
	Corrélation d'Images Volumique\hspace{0.3cm}%
	\textcolor{fontcol}{Éléments Finis Enrichis}%
}
\metasection{Enseignements}{
	Comportement des matériaux\hspace{0.3cm}%
	\textcolor{fontcol}{Géotechnique}\hspace{0.3cm}%
	Mathématiques
}

% \vspace{6pt}

%---------------------------------------------------------------------------------------
%	SUMMARAY (optional)
%----------------------------------------------------------------------------------------

\cvsection{Activités de recherche}~\\[4pt]
\singlebullet{Thématiques}{%
Je m'intéresse à comprendre et caractériser les \strong{mécanismes de rupture} de géomatériaux tels que les bétons hydrauliques, les bétons de terre (sans ciment), les roches ou les argiles.

La philosophie de mes travaux de recherche est d'effectuer des \strong{mesures de champs aux échelles fines} (microstructure, cinématique), notamment à l'aide d'\strong{imagerie 3D} (tomographie à rayon X) lors d'essais mécaniques \insitu et de faire dialoguer ces résultats avec des \strong{simulations numériques} qui prennent en compte explicitement la complexité de la microstructure des matériaux.
}

\singlebullet{Développements}{%
Je fais partie de l'équipe de développement d'un logiciel libre et open source \cite{stamati:hal-03020460} qui propose à la communauté scientifique des outils avancés de mesure de champs, notamment via l'implémentation de méthodes de corrélation d'images avancées \cite{roubin:hal-02144613, ando:hal-01992081, stavropoulou:hal-02506026}.
Je développe également une série de routines qui implémentent des enrichissements cinématiques locaux via la ``Embedded Finite Element Method'' \cite{roubin:hal-01085919, ortegalaborin:hal-03358254}.

Ces deux aspects me permettent de travailler autant sur l'\strong{analyse de champs expérimentaux} que sur des \strong{simulations numériques avancées} \cite{stamati:hal-03340858}.
}

\singlebullet{Travaux annexes}{%
	Je travaille également sur la \strong{théorie des champs aléatoires} afin de simuler l'aspect aléatoire de la géométrie et de la topologie des microstructures \cite{roubin:hal-01130430, roubin:hal-01305745}.
	Cet aspect plus fondamental de mes travaux m'entraine actuellement à monter des collaborations avec des mathématiciens \cite{roubin:hal-03170881}.
}
\vspace{3pt}

%============================================================================%
%
%	CV SECTIONS AND EVENTS (MAIN CONTENT)
%
%============================================================================%

%---------------------------------------------------------------------------------------
%	Encadrements
%----------------------------------------------------------------------------------------
\cvsection{Encadrements}~\\
\singlebullet{Thèses}{3 thèses soutenues pour un total de 145\% et 3 thèses en cours.}
\cveventsingle{2024 -}{Postdoctorat de David Georges}{UGA}{\'Etude du Comportements hydromécanique des interfaces briques/mortier de terre crue.}
\cveventsingle{2023 -}{Doctorat de Laura Perrotta}{Scuola Superiore Meridionale (Naples)}{Microstructural insights into the mechanical behavior of Lightweight Cemented Soils using X-ray microtomography.}
\cveventsingle{2022 -}{Postdoctorat de Elsa Englade}{UGA / CRAterre}{Analyse d'essais \insitu au tomographe à rayon X sur briques de terres crues: développement de méthodes de corrélation d'images globales}
\cveventsingle{2022 -}{Doctorat de Valentin Raspail (25\%)}{Université Grenoble Alpes}{Caractérisation de l'interface mortier/brique de terre crue: apport du couplage expérimental / simulation à l'échelle mésoscopique}
\cveventsingle{2022 -}{Doctorat de Guillaume Evald (25\%)}{UGA / Vinci}{Modélisation de câbles d'armatures à l'aide de méthodes Éléments Finis Enrichis}
\cveventsingle{2021 -}{Doctorat de Rida Grif (directeur de thèse, 33\%)}{UGA / Université Lille 1}{Méthodes de décomposition de domaines Globales/Locales dans le cadre de méthodes Éléments Finis Enrichis pour la modélisation de structures en béton armé}
\cveventsingle{2018 - 2021}{Doctorat de Alejandro Ortega (45\%)}{Université Grenoble Alpes}{Généralisation des E-FEM pour la modélisation de la rupture triaxiale des matériaux composites quasi fragiles à l'échelle mésoscopique}
\cveventsingle{2016 - 2020}{Doctorat de Olga Stamati (50\%)}{Université Grenoble Alpes}{Effet des hétérogénéités sur le comportement mécanique du béton à l'échelle mésoscopique : apports de la microtomographie à rayons X \insitu combinée à une modélisation E-FEM}
\cveventsingle{2016 - 2019}{Doctorat de Yue Sun (50\%)}{Université Lille 1}{Modélisation du comportement cyclique des bétons : approches mésoscopiques}
% \newpage
\vspace{10pt}

%---------------------------------------------------------------------------------------
%	Projets
%----------------------------------------------------------------------------------------
\cvsection{Projets}~\\[2pt]
\cvevent{2021 - 2024}{Décomposition de domaines appliquée aux structures du GC}{Porteur du projet}{%
	Allocation doctorale de l'IMEP$^2$ (Thèse de Rida Grif)}{%
	Environnement assuré par la collaboration avec le LaMCube (Lille 1)
}

\cvevent{2016 - 2020}{Couplage Tomographie RX / Simulations E-FEM}{Porteur du projet}{%
	Allocation doctorale de l'IMEP$^2$ (Thèse de Olga Stamati)}{%
	Environnement de 40k euros (Initiatives de Recherche Stratégiques (IRS))
}

\cvevent{2016 - }{SPAM: Software for Practical Analysis of Materials \cite{stamati:hal-03020460}}{Développeur}{%
	Développeur du logiciel libre \href{https://pypi.org/project/spam/}{SPAM} destiné à la communauté scientifique. Implémentation de méthodes de mesures de champs innovantes
	comme la corrélation volumique globale.
}{%
	Maintien, diffusion et aide aux utilisateurs ($\approx$ 400 téléchargements par mois).
}

\cvevent{2013 - 2017}{Projets nationaux et européens}{\textcolor{fontcol}{Participation}}{%
ANR : \href{https://anr.fr/Projet-ANR-13-BS09-0022}{Mesoscopic durability investigations for concrete}
portée par J.-B. Colliat.% à laquelle j'ai participé sur les aspects de simulations numériques.
}{%
ERC : \href{https://www.cimne.com/compdesmat}{Computational design of eng. materials}
portée par X. Oliver.% à laquelle j'ai participé durant mes deux années de postdoc (CIMNE, UPC Barcelone).
}
\vspace{10pt}

%---------------------------------------------------------------------------------------
%	Publications
%----------------------------------------------------------------------------------------
\cvsection{Publications}
{%
	\setlength\extrarowheight{4pt}
	\taburulecolor{acccol}
	\begin{minipage}{8cm} % total of 16cm
		\centering
		\begin{tabu}{r|c}
			% \hline
			Articles & \totalarticles \\
			\hline
			Citations / h / i$_{10}$& \totalcitations~/ \hindex~/ \iindex$^\star$\\
			\hline
			Chapitres d'ouvrages & \totalbooks\\
			\hline
			Séminaires invités & 2\\
		\end{tabu}
	\end{minipage}
	\begin{minipage}{8cm}
		\begin{tikzpicture}
			\begin{axis} [
				ybar,
				width=8cm,
				height=4cm,
				% ylabel={citations},
				% ylabel near ticks,
				x tick label style={/pgf/number format/.cd,%
				set thousands separator={{}},
				},
				xtick align=inside,
				bar width=10pt,
				axis line style={bgcol},
				% every axis label/.append style ={bgcol},
				every tick label/.append style={bgcol},
				legend style={at={(0,1)},anchor=north west},
				legend image code/.code={
				\draw [#1] (0cm,-0.1cm) rectangle (0.2cm,0.25cm);
				},
			]
				\addplot[bgcol,fill=acccol] table {var_citations.txt};
				\addlegendentry{Citations$^\star$}
			\end{axis}
		\end{tikzpicture}
		% \hfill
		\rotatebox{90}{
			\scriptsize
			$^\star$\href{https://scholar.google.com/citations?user=DSfSW7oAAAAJ}{données google scholar}
		}
	\end{minipage}
}

\vspace{5pt}

\singlebullet{Liste complète}{%
	\href{https://www.eroubin.xyz/\#publications}{https://www.eroubin.xyz/\#publications}
}
\printbibliography[heading=none]


%---------------------------------------------------------------------------------------
%	Responsabilités
%----------------------------------------------------------------------------------------
\cvsection{Responsabilités}~\\[2pt]

\cveventsimple{2025 - }{Responsable de l'équipe RV}{Responsabilité d'une équipe d'une dixaine de permanents. Lien avec la direction du laboratoire, évaluation HCERES, animation scientifique.}
\cveventsimple{2024 - }{Suppléant au CNU}{Participation à l'AG de 2024 et la session avancement de 2025.}
\cveventsimple{2023 - }{Direction des études (poursuites et réorientation)}{Lien avec la direction du laboratoire, évaluation HCERES, animation scientifique.}
\cveventsimple{2018 - }{Webmaster}{Création, maintien et mise à jour du site web du laboratoire.}
\cveventsimple{2018 - 2024}{Animation et communication scientifique}{Organisation et diffusion des séminaires du laboratoire. Mise en place de réunions doctorants / post-doctorants.}
\cveventsimple{2018 - 2022}{Conseil Scientifique}{Membre élu du conseil scientifique du laboratoire 3SR.}
\cveventsimple{2016 - 2021}{Directeur des études (45h éq. TD)}{Interface entre les 250 étudiants de la filière et les équipes pédagogique et administrative de l'IUT. Développement et maintien d'une application web pour la gestion des absences (django)}~\\[2pt]

\cveventsimple{~}{Comités de sélection}{Participation à trois comités de sélection de maître de conférences dont un en tant qu'extérieur.}

      
\vspace{10pt}

%---------------------------------------------------------------------------------------
%	Enseignements
%----------------------------------------------------------------------------------------
\cvsection{Principaux Enseignements}~\\[2pt]
\singlebullet{Volume horaire}{%
Une moyenne de $\approx$ 200 heures éq. TD sans compter les décharges administratives.
}

\cveventsingle{2021 -}{Introduction aux matériaux du Génie Civil (25\%)}{IUT GCCD / UGA}{%
	Mise en place de ce nouveau module (CM/TD/TP) dont je suis le responsable, qui introduit les principaux matériaux du Génie Civil en posant les bases de leur comportement mécanique (élasticité, plasticité, \dots).
}
\cveventsingle{2019 -}{Mathématiques pour le technicien (45\%)}{IUT GCCD / UGA}{%
	En plus de la responsabilité d'un des modules et d'interventions dans les autres, j'ai introduit les TP de programmation (python) pour chacun d'eux.}
\cveventsingle{2015 -}{Géotechnique pour le technicien (25\%)}{IUT1 GCCD / UGA}{%
	Intervenant dans les différents modules de géotechnique (tassements / dimensionnement de fondations).
}
\cveventsingle{2010 - 2013}{Vacations diverses (184h éq. TD)}{ENS Cachan / Paris VI}{%
	Probabilité et incertitudes, Mécanique des Milieux Continus, Méthodes numériques, Mécanique probabiliste.
}

%============================================================================%
%
%
%
%	DOCUMENT END
%
%
%
%============================================================================%
\end{document}
